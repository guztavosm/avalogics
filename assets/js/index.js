$(document).on("ready", function(){
	$(".item").on("click", function(){
		$this = $(this);
		$.ajax("assets/js/portfolio.json", {dataType: "json"})
		.done(function(data){
			var index = $this.attr("data-index")
			$("#project-title").html(data[index].project)
			$("#project").show("fast")
		})
	})
})